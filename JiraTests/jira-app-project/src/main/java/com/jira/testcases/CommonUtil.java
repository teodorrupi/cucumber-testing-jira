package com.jira.testcases;

import java.io.IOException;

import org.junit.Assert;

import com.jira.util.WebConnector;
import com.thoughtworks.selenium.Selenium;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

/*Common util class contains common methods which are used in several features*/
public class CommonUtil {

	
WebConnector Selenium = WebConnector.getInstance();
	
	@Given ("^RunMode is \"([^\"]*)\"$")
	public void checkRunMode(String runmode){
		if(runmode.equals("N")){
			throw new PendingException("Skipping the test dataset cause runmode = " + runmode);
		}
	}
	
	//And I am on the home page in "<browserType>"
	@Given ("^I am on the home page in \"([^\"]*)\"$") 
	public void loginWithDefaultCredentials (String browser)  {	
		String result = Selenium.doDefaultLogin(browser);	
		Assert.assertEquals(result, Selenium.PASS);
	}
	
	//Then I log out
	@And ("^I sign out$") 
	public void signOut ()  {
		String result = Selenium.logOut();
		Assert.assertEquals(result, Selenium.PASS);
		
	}
		
}